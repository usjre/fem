CC=gcc
LD=gcc
CFLAGS=-g -Wall -Wextra -pedantic -std=c11
CPPFLAGS=
LDFLAGS=-g
LDLIBS=-lm

.PHONY: all clean

all: fem

clean:
	rm -f fem fem.o

fem: fem.o
	$(LD) $(LDFLAGS) -o $@ $^ $(LDLIBS)

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $< $(CPPFLAGS)
