#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <tgmath.h>
#include <assert.h>

#define FEM_PI 3.14159265358979323846

#define MAX(a,b) ((a)>(b)?(a):(b))

typedef double fem_scalar;

struct node {
	fem_scalar x;
	fem_scalar y;
	bool onBoundary;
};

struct element {
	size_t     nodes[3];
	fem_scalar stiffness[3*3];
	fem_scalar determinant;
	fem_scalar transform[2*2];
	fem_scalar affinity[2];
};

struct incidence {
	size_t element;
	int    index;
};

struct mesh {
	size_t            numNodes;
	size_t            numElements;
	struct node      *nodes;
	struct element   *elements;
	size_t           *incidenceOffsets;
	struct incidence *incidenceList;
};

void
ref2actual(const struct element *element, const fem_scalar in[2], fem_scalar out[2])
{
	out[0] = element->transform[0] * in[0] + element->transform[2] * in[1] + element->affinity[0];
	out[1] = element->transform[1] * in[0] + element->transform[3] * in[1] + element->affinity[1];
}

void
generate_mesh(struct mesh *mesh, size_t numNodesX, size_t numNodesY)
{
	mesh->numNodes = numNodesX * numNodesY;
	mesh->numElements = (numNodesX - 1) * (numNodesY - 1) * 2;
	mesh->nodes = calloc(mesh->numNodes, sizeof *mesh->nodes);
	mesh->elements = calloc(mesh->numElements, sizeof *mesh->elements);

	// Lay out nodes in a grid
	size_t n = 0;
	for (size_t y = 0; y < numNodesY; y++) {
		for (size_t x = 0; x < numNodesX; x++) {
			mesh->nodes[n].x = (fem_scalar)x / (numNodesX - 1);
			mesh->nodes[n].y = (fem_scalar)y / (numNodesY - 1);
			mesh->nodes[n].onBoundary = x == 0 || y == 0 || x == numNodesX - 1 || y == numNodesY - 1;
			n++;
		}
	}

	// Set up two triangles (one rectangle) for each grid cell
	size_t e = 0;
	for (size_t y = 0; y < numNodesY-1; y++) {
		for (size_t x = 0; x < numNodesX-1; x++) {
			mesh->elements[e].nodes[0] = x + numNodesX * y;
			mesh->elements[e].nodes[1] = (x+1) + numNodesX * y;
			mesh->elements[e].nodes[2] = x + numNodesX * (y+1);
			e++;

			mesh->elements[e].nodes[0] = (x+1) + numNodesX * (y+1);
			mesh->elements[e].nodes[1] = x + numNodesX * (y+1);
			mesh->elements[e].nodes[2] = (x+1) + numNodesX * y;
			e++;
		}
	}
}

void
build_incidence_list(struct mesh *mesh)
{
	// calculate # of incident elements per node
	size_t *lengths = calloc(mesh->numNodes, sizeof *lengths);
	for (size_t e = 0; e < mesh->numElements; e++) {
		const struct element *element = &mesh->elements[e];
		lengths[element->nodes[0]]++;
		lengths[element->nodes[1]]++;
		lengths[element->nodes[2]]++;
	}

	// cumulative sum over lengths to build offsets into global list
	mesh->incidenceOffsets = calloc(mesh->numNodes + 1,
		sizeof *mesh->incidenceOffsets);
	for (size_t n = 0; n < mesh->numNodes; n++) {
		mesh->incidenceOffsets[n+1] = mesh->incidenceOffsets[n] + lengths[n];
	}

	// build global list of incidences
	size_t *cursors = lengths;
	memcpy(cursors, mesh->incidenceOffsets, mesh->numNodes * sizeof *cursors);
	mesh->incidenceList = calloc(mesh->incidenceOffsets[mesh->numNodes],
		sizeof *mesh->incidenceList);
	for (size_t e = 0; e < mesh->numElements; e++) {
		const struct element *element = &mesh->elements[e];
		mesh->incidenceList[cursors[element->nodes[0]]++] = (struct incidence){ e, 0 };
		mesh->incidenceList[cursors[element->nodes[1]]++] = (struct incidence){ e, 1 };
		mesh->incidenceList[cursors[element->nodes[2]]++] = (struct incidence){ e, 2 };
	}

	free(lengths);
}

void
calculate_stiffness(struct mesh *mesh)
{
	for (size_t e = 0; e < mesh->numElements; e++) {
		struct element *element = &mesh->elements[e];
		const struct node *nodeA = &mesh->nodes[element->nodes[0]];
		const struct node *nodeB = &mesh->nodes[element->nodes[1]];
		const struct node *nodeC = &mesh->nodes[element->nodes[2]];

		element->transform[0] = nodeB->x - nodeA->x;
		element->transform[1] = nodeB->y - nodeA->y;
		element->transform[2] = nodeC->x - nodeA->x;
		element->transform[3] = nodeC->y - nodeA->y;

		element->affinity[0] = nodeA->x;
		element->affinity[1] = nodeA->y;

		element->determinant =
			element->transform[0] * element->transform[3] -
			element->transform[1] * element->transform[2];
		
		fem_scalar invTransform[4];
		invTransform[0] =  element->transform[3] / element->determinant;
		invTransform[1] = -element->transform[2] / element->determinant;
		invTransform[2] = -element->transform[1] / element->determinant;
		invTransform[3] =  element->transform[0] / element->determinant;

		fem_scalar gradBasis[3][2];
		gradBasis[0][0] = -1.0 * invTransform[0] + -1.0 * invTransform[1];
		gradBasis[0][1] = -1.0 * invTransform[2] + -1.0 * invTransform[3];
		gradBasis[1][0] = 1.0 * invTransform[0];
		gradBasis[1][1] = 1.0 * invTransform[2];
		gradBasis[2][0] = 1.0 * invTransform[1];
		gradBasis[2][1] = 1.0 * invTransform[3];

		element->determinant = fabs(element->determinant);

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				element->stiffness[3*i+j] = (gradBasis[i][0] * gradBasis[j][0] + gradBasis[i][1] * gradBasis[j][1]) * element->determinant / 2.0;
			}
		}
	}
}

void
calculate_constants(struct mesh *mesh, fem_scalar (*function)(fem_scalar x, fem_scalar y), fem_scalar *constants)
{
	// TODO more sample points & weights!
	const int SAMPLE_COUNT = 3;
	const fem_scalar samplePoints[][2] = { {0.5, 0.0}, {0.0, 0.5}, {0.5, 0.5} };
	const fem_scalar sampleWeights[] = { 1.0/6.0, 1.0/6.0, 1.0/6.0 };

	for (size_t e = 0; e < mesh->numElements; e++) {
		struct element *element = &mesh->elements[e];

		fem_scalar integralA = 0.0;
		fem_scalar integralB = 0.0;
		fem_scalar integralC = 0.0;

		fem_scalar samples[SAMPLE_COUNT];
		for (int i = 0; i < SAMPLE_COUNT; i++) {
			fem_scalar point[2];
			ref2actual(element, samplePoints[i], point);
			samples[i] = function(point[0], point[1]);

			integralA += sampleWeights[i] *
				(1.0 - samplePoints[i][0] - samplePoints[i][1]) * samples[i];
			integralB += sampleWeights[i] *
				samplePoints[i][0] * samples[i];
			integralC += sampleWeights[i] *
				samplePoints[i][1] * samples[i];
		}

		constants[element->nodes[0]] += integralA * element->determinant;
		constants[element->nodes[1]] += integralB * element->determinant;
		constants[element->nodes[2]] += integralC * element->determinant;
	}
}

void
solve_poisson(struct mesh *mesh, fem_scalar *constants, fem_scalar (*boundary)(fem_scalar x, fem_scalar y), fem_scalar *solution)
{
	memset(solution, 0, mesh->numNodes * sizeof *solution);
	for (size_t n = 0; n < mesh->numNodes; n++) {
		const struct node *node = &mesh->nodes[n];
		if (node->onBoundary) {
			solution[n] = boundary(node->x, node->y);
		}
	}
	size_t it;
	for (it = 0; it < 10000; it++) {
		fem_scalar progress = 0.0;
		for (size_t n = 0; n < mesh->numNodes; n++) {

			if (mesh->nodes[n].onBoundary) continue;

			fem_scalar oldSolution = solution[n];

			assert(!isnan(solution[n]));
			fem_scalar divisor = 0.0;
			solution[n] = constants[n];

			size_t low  = mesh->incidenceOffsets[n];
			size_t high = mesh->incidenceOffsets[n+1];
			assert(low < high);
			for (size_t o = low; o < high; o++) {
				struct incidence incidence = mesh->incidenceList[o];
				const struct element *element = &mesh->elements[incidence.element];
				int i = incidence.index;
				int j = (i + 1) % 3;
				int k = (i + 2) % 3;
				solution[n] -= element->stiffness[3*i+j] *
					solution[element->nodes[j]];
				assert(!isnan(solution[n]));
				solution[n] -= element->stiffness[3*i+k] *
					solution[element->nodes[k]];
				assert(!isnan(solution[n]));
				divisor += element->stiffness[3*i+i];
			}

			assert(divisor > 0.0);
			assert(!isnan(solution[n] / divisor));
			solution[n] /= divisor;

			progress = MAX(progress, fabs(solution[n] - oldSolution));
		}
		printf("%lf\n", progress);
		if (progress < 0.000005) break;
	}
	printf("took %zu iterations\n", it);
}

void
free_mesh(struct mesh *mesh)
{
	free(mesh->nodes);
	free(mesh->elements);
	free(mesh->incidenceOffsets);
	free(mesh->incidenceList);
}

void
write_nodes(const struct mesh *mesh, const char *filename)
{
	FILE *file = fopen(filename, "w");
	for (size_t n = 0; n < mesh->numNodes; n++) {
		fprintf(file, "%lf,%lf\n", mesh->nodes[n].x, mesh->nodes[n].y);
	}
	fclose(file);
}

void
write_solution(size_t numNodes, const fem_scalar *solution, const char *filename)
{
	FILE *file = fopen(filename, "w");
	for (size_t n = 0; n < numNodes; n++) {
		fprintf(file, "%lf\n", solution[n]);
	}
	fclose(file);
}

fem_scalar
my_function(fem_scalar x, fem_scalar y)
{
	return 2.0 * FEM_PI * FEM_PI * cos(FEM_PI * (x + y));
}

fem_scalar
my_boundary(fem_scalar x, fem_scalar y)
{
	if (x == 0.0) {
		return cos(FEM_PI * y);
	}
	if (y == 0.0) {
		return cos(FEM_PI * x);
	}
	if (x == 1.0) {
		return cos(FEM_PI * (y+1.0));
	}
	if (y == 1.0) {
		return cos(FEM_PI * (x+1.0));
	}
	return NAN;
}

int
main()
{
	struct mesh mesh;
	generate_mesh(&mesh, 100, 100);
	build_incidence_list(&mesh);
	write_nodes(&mesh, "nodes.csv");
	fem_scalar *constants = calloc(mesh.numNodes, sizeof *constants);
	calculate_stiffness(&mesh);
	calculate_constants(&mesh, my_function, constants);
	fem_scalar *solution = calloc(mesh.numNodes, sizeof *solution);
	solve_poisson(&mesh, constants, my_boundary, solution);
	write_solution(mesh.numNodes, solution, "solution.csv");
	free(constants);
	free(solution);
	free_mesh(&mesh);
	return 0;
}
